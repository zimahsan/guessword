Rails.application.routes.draw do
  resources :questions
  resources :games
  
  devise_for :players
  root to: redirect('/players/sign_in')
  
  

  resources :chat_rooms, only: [:new, :create, :show, :index]
  #root 'chat_rooms#index'

  mount ActionCable.server => '/cable'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
