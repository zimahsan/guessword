class CreateQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :questions do |t|
      t.string :question_field
      t.string :question_answer
      t.string :player_answer
      t.string :game_id

      t.timestamps
    end
  end
end
