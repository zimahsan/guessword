json.extract! game, :id, :image, :game_name, :created_at, :updated_at
json.url game_url(game, format: :json)
