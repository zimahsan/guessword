json.extract! question, :id, :question_field, :question_answer, :player_answer, :game_id, :created_at, :updated_at
json.url question_url(question, format: :json)
